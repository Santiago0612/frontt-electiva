import axios from 'axios'


export const SendData = async (data) => {
  try {
    const response = await axios.post('http://localhost:8000/exam/cafe', data);
    return response.data;
  }catch (error){
    console.log(error);
    throw error;
  }


}

export default SendData
