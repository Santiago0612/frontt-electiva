import axios from "axios";

export async function ApiGet(){
    try {
      const response = await axios.get('http://localhost:8000/exam/cafe');
      return response.data;
    }catch (error){
      console.log(error);
    }
}