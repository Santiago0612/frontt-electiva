import { useForm } from "react-hook-form";
import {SendData} from  "../api/Api-register";
import {  useNavigate } from "react-router";
import toast from 'react-hot-toast'

const Form = () => {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();  

  const onSubmit = async (data) => {
    try {
        const datas = await SendData(data);
        console.log(datas )
        toast.success('Registro exitoso')
        navigate("/add")

     }catch (error) {
      console.log(error + "ads");
      const dataError = error.response.data;
      console.log(dataError);
      
      const keys = Object.keys(dataError)
      for (let i =0 ; i < keys.length; i++){
        const key = keys[i]
        const value = dataError[key]
        console.log(value)
        toast.error(`${key}: ${value}`)
      
    }
  }
  }
  return (
    <div className="flex flex-col items-center">
      <h2 className="text-3xl font-bold mb-4 text-white">
        Formulario de registro
      </h2>
      <form onSubmit={handleSubmit(onSubmit)} className="w-full max-w-lg">
        <div className="flex flex-wrap mb-6">
          <label className="block text-yellow-400 text-sm font-bold mb-2">
            Nombre
          </label>
          <input
            {...register("nombre")}
            className="focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-opacity-50 transform hover:scale-105 border border-gray-300 py-2 px-4 w-full rounded-md 
  transition-all duration-150 ease-in-out shadow-sm
  hover:shadow-md hover:ring-2 hover:ring-gradient-to-br from-purple-400 to-pink-500"
            placeholder="Nombre del cafe"
            type="text"
          ></input>
        </div>
        <div className="flex flex-wrap mb-6">
          <label className="block text-yellow-400 text-sm font-bold mb-2">
            Tipo
          </label>
          <input
            {...register("tipo")}
            className="focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-opacity-50 transform hover:scale-105 border border-gray-300 py-2 px-4 w-full rounded-md 
  transition-all duration-150 ease-in-out shadow-sm
  hover:shadow-md hover:ring-2 hover:ring-gradient-to-br from-purple-400 to-pink-500 b"
            type="text"
          ></input>
        </div>
        <div className="flex flex-wrap mb-6">
          <label className="block text-yellow-400 text-sm font-bold mb-2">
            Region
          </label>
          <input
            {...register("region")}
            className="focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-opacity-50 transform hover:scale-105 border border-gray-300 py-2 px-4 w-full rounded-md 
  transition-all duration-150 ease-in-out shadow-sm
  hover:shadow-md hover:ring-2 hover:ring-gradient-to-br from-purple-400 to-pink-500 b"
            type="text"
          ></input>
        </div>
        <div className="flex justify-center">
          <button
            type="submit"
            value="enviar"
            className="transform transition-all duration-200 hover:-translate-y-1 focus:outline-none bg-gradient-to-r from-yellow-700 to-yellow-200 text-white font-semibold rounded-md py-2 px-6 shadow-md hover:shadow-lg"
          >
            Registrar
          </button>
        </div>
      </form>
    </div>
  );
};

export default Form;
