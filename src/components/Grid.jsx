import { useEffect, useState } from "react";
import { ApiGet } from "../api/Api_get";
import { NavLink } from "react-router-dom";



const Grid = () => {
  const [data, setData] = useState([]);
 
  useEffect(() => {
    async function fetchData() {
      const response = await ApiGet();
      setData(response);
    }
    fetchData();
  }, []);

const tipoCount = data.reduce((count, item) => {
    count[item.tipo] = (count[item.tipo] || 0) + 1;
    return count;
}, {});

  return (
    <>
    
    <div style={{overflowY: 'hidden'}}>
    <table className=" min-w-full divide-y divide-gray-200 mt-20 ">
      <thead>
        <tr>
          <th className=" bg-yellow-950 px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider rounded-tl-md">
            Nombre
          </th>
          <th className=" bg-yellow-950 px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider">
            Tipo
          </th>
          <th className=" bg-yellow-950 px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider">
            Region
          </th>
        </tr>
      </thead>
      <tbody className='bg-white divide-y divide-gray-200'>
        {data.map((item) => (
          <tr key={item.id} className={'bg-gradient-to-b from-yellow-800 to-yellow-300"'}>
            <td className='  px-6 py-4 whitespace-nowrap'>
              <div className='text-sm font-medium text-gray-900'>{item.nombre}</div>
            </td>
            <td className='  px-6 py-4 whitespace-nowrap'>
              <div className='text-sm font-medium text-gray-900'>{item.tipo}</div>
            </td>
            <td className=' px-6 py-4 whitespace-nowrap'>
              <div className='text-sm font-medium text-gray-900'>{item.region}</div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>

    </div>
   
    <div className="justify-center flex mt-8 ">
        

          <NavLink to={'/'} className="transform transition-all duration-200 hover:-translate-y-1 focus:outline-none bg-gradient-to-r from-yellow-700 to-yellow-200 text-white font-semibold rounded-md py-2 px-6 shadow-md hover:shadow-lg">Volver</NavLink>
          
    </div>
    <div className="flex flex-col items-center  border-slate-50/10 bg-white bg-opacity-0 backdrop-blur-md transition-colors duration-500 border-2 mt-4">
    <h2 className="text-lg font-semibold text-yellow-700">Tipos</h2>
           <ul className="mt-2 space-y-2">
            {Object.entries(tipoCount).map(([type, count]) => (
              <li key={type} className="text-yellow-400 font-bold text-3xl">
                {type}: {count}

              </li>
            ))}

           </ul>
           </div>
    </>

  );
};

export default Grid;
