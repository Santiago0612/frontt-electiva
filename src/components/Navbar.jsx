import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div>
      <nav className=" fixed z-50 pt-10 pb-4 w-screen border-b border-slate-50/10 bg-white bg-opacity-0 backdrop-blur-md transition-colors duration-500 flex">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="flex justify-between h-16  font-bold text-white text-4xl">
            Coffe Register Demo

          </div>
          <div className=" flex justify-around  ">
              <NavLink to={'/register'} className="text-white font-semibold text-xl transition duration-300 ease-in-out hover:text-2xl   ">Registro</NavLink>
              <NavLink to={'/add'} className="text-white font-semibold text-xl transition duration-300 ease-in-out hover:text-2xl">Listado</NavLink>
            </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
