/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
import { Navigate, Outlet, Route, Routes } from 'react-router-dom'
import Menu from './pages/Menu'
import Grid from './components/Grid'
import Form from './components/Form'

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Menu />} />
      <Route path="*" element={<Navigate to="/" />} />
      <Route path="add" element={<Grid/>}/>
      <Route path="register" element={<Form/>}/>
    </Routes>
  )
}

export default Router